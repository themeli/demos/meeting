package gr.demokritos.meeting;

public class Availability {
    private User user;
    private Timezone timezone;
    private Day day;
    private Boolean isAvailable;

    public Availability() {

    }

    public Availability(User user, Timezone timezone, Day day, Boolean isAvailable) {
        this.user = user;
        this.timezone = timezone;
        this.day = day;
        this.isAvailable = isAvailable;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Timezone getTimezone() {
        return timezone;
    }

    public void setTimezone(Timezone timezone) {
        this.timezone = timezone;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Boolean getAvailable() {
        return isAvailable;
    }

    public void setAvailable(Boolean available) {
        isAvailable = available;
    }
}
