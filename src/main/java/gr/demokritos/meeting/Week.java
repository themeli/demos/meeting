package gr.demokritos.meeting;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Week {
    private Integer weekNumber;
    private Set<User> users = new HashSet<>();
    private Day monday;
    private Day tuesday;
    private Day wednesday;
    private Day thursday;
    private Day friday;

    private List<Day> workingWeek = new ArrayList<>();

    public Week() {

    }

    public Week(Integer weekNumber, Set<User> users, Day monday, Day tuesday, Day wednesday, Day thursday, Day friday) {
        this.weekNumber = weekNumber;
        this.users = users;
        this.monday = monday;
        this.tuesday = tuesday;
        this.wednesday = wednesday;
        this.thursday = thursday;
        this.friday = friday;
    }

    public void initWeek(LocalDate today, Integer duration) {
        switch (today.getDayOfWeek()) {
            case MONDAY:
                todayIsMonday(today, duration);
                break;
            case TUESDAY:
                todayIsTuesday(today, duration);
                break;
            case WEDNESDAY:
                todayIsWednesday(today, duration);
                break;
            case THURSDAY:
                todayIsThursday(today, duration);
                break;
            case FRIDAY:
                todayIsFriday(today, duration);
                break;
            case SATURDAY:
                today = today.plus(2, ChronoUnit.DAYS);
                todayIsMonday(today, duration);
                break;
            case SUNDAY:
                today = today.plus(1, ChronoUnit.DAYS);
                todayIsMonday(today, duration);
                break;
        }
        workingWeek.add(monday);
        workingWeek.add(tuesday);
        workingWeek.add(wednesday);
        workingWeek.add(thursday);
        workingWeek.add(friday);
    }

    private void todayIsMonday(LocalDate today, Integer duration) {
        this.monday = new Day(today, today.getDayOfWeek(), duration);
        this.tuesday = new Day(monday.getDate().plus(1, ChronoUnit.DAYS), monday.getDate().plus(1, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.wednesday = new Day(monday.getDate().plus(2, ChronoUnit.DAYS), monday.getDate().plus(2, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.thursday = new Day(monday.getDate().plus(3, ChronoUnit.DAYS), monday.getDate().plus(3, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.friday = new Day(monday.getDate().plus(4, ChronoUnit.DAYS), monday.getDate().plus(4, ChronoUnit.DAYS).getDayOfWeek(), duration);
    }

    private void todayIsTuesday(LocalDate today, Integer duration) {
        this.tuesday = new Day(today, today.getDayOfWeek(), duration);
        this.monday = new Day(this.tuesday.getDate().minus(1, ChronoUnit.DAYS), this.tuesday.getDate().minus(1, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.wednesday = new Day(this.tuesday.getDate().plus(1, ChronoUnit.DAYS), this.tuesday.getDate().plus(1, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.thursday = new Day(this.tuesday.getDate().plus(2, ChronoUnit.DAYS), this.tuesday.getDate().plus(2, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.friday = new Day(this.tuesday.getDate().plus(3, ChronoUnit.DAYS), this.tuesday.getDate().plus(3, ChronoUnit.DAYS).getDayOfWeek(), duration);
    }

    private void todayIsWednesday(LocalDate today, Integer duration) {
        this.wednesday = new Day(today, today.getDayOfWeek(), duration);
        this.monday = new Day(this.wednesday.getDate().minus(2, ChronoUnit.DAYS), this.wednesday.getDate().minus(2, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.tuesday = new Day(this.wednesday.getDate().minus(1, ChronoUnit.DAYS), this.wednesday.getDate().minus(1, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.thursday = new Day(this.wednesday.getDate().plus(1, ChronoUnit.DAYS), this.wednesday.getDate().plus(1, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.friday = new Day(this.wednesday.getDate().plus(2, ChronoUnit.DAYS), this.wednesday.getDate().plus(2, ChronoUnit.DAYS).getDayOfWeek(), duration);
    }

    private void todayIsThursday(LocalDate today, Integer duration) {
        this.thursday = new Day(today, today.getDayOfWeek(), duration);
        this.monday = new Day(this.thursday.getDate().minus(3, ChronoUnit.DAYS), this.thursday.getDate().minus(3, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.tuesday = new Day(this.thursday.getDate().minus(2, ChronoUnit.DAYS), this.thursday.getDate().minus(2, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.wednesday = new Day(this.thursday.getDate().minus(1, ChronoUnit.DAYS), this.thursday.getDate().minus(1, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.friday = new Day(this.thursday.getDate().plus(1, ChronoUnit.DAYS), this.thursday.getDate().plus(1, ChronoUnit.DAYS).getDayOfWeek(), duration);
    }

    private void todayIsFriday(LocalDate today, Integer duration) {
        this.friday = new Day(today, today.getDayOfWeek(), duration);
        this.monday = new Day(this.friday.getDate().minus(4, ChronoUnit.DAYS), this.friday.getDate().minus(4, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.tuesday = new Day(this.friday.getDate().minus(3, ChronoUnit.DAYS), this.friday.getDate().minus(3, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.wednesday = new Day(this.friday.getDate().minus(2, ChronoUnit.DAYS), this.friday.getDate().minus(2, ChronoUnit.DAYS).getDayOfWeek(), duration);
        this.thursday = new Day(this.friday.getDate().minus(1, ChronoUnit.DAYS), this.friday.getDate().minus(1, ChronoUnit.DAYS).getDayOfWeek(), duration);
    }

    public Integer getWeekNumber() {
        return weekNumber;
    }

    public void setWeekNumber(Integer weekNumber) {
        this.weekNumber = weekNumber;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public Day getMonday() {
        return monday;
    }

    public void setMonday(Day monday) {
        this.monday = monday;
    }

    public Day getTuesday() {
        return tuesday;
    }

    public void setTuesday(Day tuesday) {
        this.tuesday = tuesday;
    }

    public Day getWednesday() {
        return wednesday;
    }

    public void setWednesday(Day wednesday) {
        this.wednesday = wednesday;
    }

    public Day getThursday() {
        return thursday;
    }

    public void setThursday(Day thursday) {
        this.thursday = thursday;
    }

    public Day getFriday() {
        return friday;
    }

    public void setFriday(Day friday) {
        this.friday = friday;
    }

    public List<Day> getWorkingWeek() {
        return workingWeek;
    }

    public void setWorkingWeek(List<Day> workingWeek) {
        this.workingWeek = workingWeek;
    }
}
