package gr.demokritos.meeting;

import java.util.HashSet;
import java.util.Set;

public class PossibleMeeting implements Comparable<PossibleMeeting>{

    private Day day;
    private Timezone timezone;
    private Set<User> attendingUsers = new HashSet<>();
    private Set<User> notAttendingUsers = new HashSet<>();

    public PossibleMeeting() {

    }

    public PossibleMeeting(Day day, Timezone timezone, Set<User> attendingUsers) {
        this.day = day;
        this.timezone = timezone;
        this.attendingUsers = attendingUsers;
    }

    public Day getDay() {
        return day;
    }

    public void setDay(Day day) {
        this.day = day;
    }

    public Timezone getTimezone() {
        return timezone;
    }

    public void setTimezone(Timezone timezone) {
        this.timezone = timezone;
    }

    public Set<User> getAttendingUsers() {
        return attendingUsers;
    }

    public void setAttendingUsers(Set<User> attendingUsers) {
        this.attendingUsers = attendingUsers;
    }

    public Set<User> getNotAttendingUsers() {
        return notAttendingUsers;
    }

    public void setNotAttendingUsers(Set<User> notAttendingUsers) {
        this.notAttendingUsers = notAttendingUsers;
    }

    @Override
    public int compareTo(PossibleMeeting possibleMeeting) {
        return -(this.getAttendingUsers().size() - possibleMeeting.getAttendingUsers().size());
    }
}
